package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.dao.AnneeDAO;
import modele.dao.ProfessionnelDAO;
import modele.metier.Annee;
import modele.metier.Professionnel;
import vue.VueContactAccordJury;

/**
 * Controlleur AccordJurry
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class ContactAccordJuryControlleur implements WindowListener, ActionListener {

    private VueContactAccordJury vue; // Vue
    private final GeneralControlleur ctrlGeneral;
    private String spe;

    public ContactAccordJuryControlleur(VueContactAccordJury vue, GeneralControlleur ctrl) throws SQLException {
        this.vue = vue;
        this.ctrlGeneral = ctrl;

        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);

        // le contrôleur écoute les actions des boutons et des radio boutons
        this.vue.getjButtonRefresh().addActionListener(this);
        this.vue.getjButtonRetour().addActionListener(this);
        this.vue.getjRadioButtonTous().addActionListener(this);
        this.vue.getjRadioButtonSISR().addActionListener(this);
        this.vue.getjRadioButtonSLAM().addActionListener(this);

        // préparer l'état initial de la vue
        ArrayList<Annee> lesAnnees = AnneeDAO.selectAll();
        if (!lesAnnees.isEmpty()) {

            //Remise à zéro des JComboBox
            this.vue.getjComboBoxAnneeDebut().setModel(new javax.swing.DefaultComboBoxModel());
            this.vue.getjComboBoxAnneeFin().setModel(new javax.swing.DefaultComboBoxModel());
            //Remplissage des JComboBox avec les années
            for (Annee uneAnne : lesAnnees) {
                this.vue.getjComboBoxAnneeDebut().addItem(Integer.toString(uneAnne.getDateAnnee()));
                this.vue.getjComboBoxAnneeFin().addItem(Integer.toString(uneAnne.getDateAnnee()));
            }
            //Sélection des Années de début et de fin par défaut
            this.vue.getjComboBoxAnneeDebut().setSelectedIndex(0);
            this.vue.getjComboBoxAnneeFin().setSelectedIndex(this.vue.getjComboBoxAnneeDebut().getItemCount() - 1);
            //Appel du remplissage de la Table Jury
            afficherLesProfessionnelsJury();
        }
    }

    /**
     * Remplissage de la table Jury
     */
    private void afficherLesProfessionnelsJury() {
        ArrayList<Professionnel> lesProfessionnels;
        JFrame frame = new JFrame("showMessageDialog");
        try {
            //Si Année début inférieur à Année fin
            if (Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()) - Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()) <= 0) {
                //Si le Radio Bouton Tous est sélectionné
                if (getVue().getjRadioButtonTous().isSelected()) {
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordAnnee(Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()));
                } else { //Si un autre radio Bouton est sélectionné
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordAnneeSpe(Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()), spe);
                }
                vue.getModeleTableJury().setRowCount(0);
                String[] titresColonnes = {"Professionnel", "Téléphone Professionnel", "Email Professionnel", "Entreprise", "Adresse Entreprise", "Téléphone Entreprise"};
                vue.getModeleTableJury().setColumnIdentifiers(titresColonnes);
                String[] ligneDonnees = new String[6];
                for (Professionnel unProfessionnel : lesProfessionnels) {
                    ligneDonnees[0] = unProfessionnel.getNom() + " " + unProfessionnel.getPrenom();
                    ligneDonnees[1] = unProfessionnel.getTel();
                    ligneDonnees[2] = unProfessionnel.getMail();
                    ligneDonnees[3] = unProfessionnel.getEntreprise().getNom();
                    ligneDonnees[4] = unProfessionnel.getEntreprise().getAdr() + ", " + unProfessionnel.getEntreprise().getCp() + " " + unProfessionnel.getEntreprise().getVille();
                    ligneDonnees[5] = unProfessionnel.getEntreprise().getTel();
                    vue.getModeleTableJury().addRow(ligneDonnees);
                }
                //Si la Table Jury ne retourne aucune ligne
                if (vue.getModeleTableJury().getRowCount() == 0) {
                    JOptionPane.showMessageDialog(frame, "Aucune donnée pour ces paramètres", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
                // Redimensionner table en fonction de son contenu
                ctrlGeneral.resizeColumnWidth(vue.getjTableJury());
            } else { //Si Année début supérieur à Année fin
                JOptionPane.showMessageDialog(frame, "Année début supérieur à Année fin", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getVue(), "CtrlLesAccordsJury - échec de sélection des Accords Jury, erreur : " + ex);
        }
    }

    // ACCESSEURS et MUTATEURS
    public VueContactAccordJury getVue() {
        return vue;
    }

    public void setVue(VueContactAccordJury vue) {
        this.vue = vue;
    }

    //Méthodes Abstraites (WindowListener)
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        ctrlGeneral.quitterApplication();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    //Méthode Abstraite (ActionListener)
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonRetour())) { //Bouton Retour
            ctrlGeneral.afficherMain();
        }
        if (e.getSource().equals(vue.getjButtonRefresh())) { //Bouton Rechercher / Rafraichir
            afficherLesProfessionnelsJury();
        }
        if (e.getSource().equals(vue.getjRadioButtonSLAM())) { //RadioBouton SLAM
            spe = "SLAM";
        }
        if (e.getSource().equals(vue.getjRadioButtonSISR())) { //RadioBouton SISR
            spe = "SISR";
        }

    }
}
