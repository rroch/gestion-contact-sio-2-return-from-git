package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modele.dao.AnneeDAO;
import modele.dao.ProfessionnelDAO;
import modele.metier.Annee;
import modele.metier.Professionnel;
import vue.VueContactAccordStagiaires;

/**
 * Controlleur ContactAccordStagiaires
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class ContactAccordStagiairesControlleur implements WindowListener, ActionListener {

    private VueContactAccordStagiaires vue; // Vue
    private final GeneralControlleur ctrlGeneral;
    private String spe;
    private String perio;

    public ContactAccordStagiairesControlleur(VueContactAccordStagiaires vue, GeneralControlleur ctrl) throws SQLException {
        this.vue = vue;
        this.ctrlGeneral = ctrl;

        // le contrôleur écoute la vue
        this.vue.addWindowListener(this);

        // le contrôleur écoute les actions des boutons et des radio boutons
        this.vue.getjButtonRefresh().addActionListener(this);
        this.vue.getjButtonRetour().addActionListener(this);
        this.vue.getjRadioButtonToutePeriode().addActionListener(this);
        this.vue.getjRadioButtonPremierSemestre().addActionListener(this);
        this.vue.getjRadioButtonSecondSemestre().addActionListener(this);
        this.vue.getjRadioButtonTouteSpecialite().addActionListener(this);
        this.vue.getjRadioButtonSISR().addActionListener(this);
        this.vue.getjRadioButtonSLAM().addActionListener(this);

        // préparer l'état initial de la vue
        ArrayList<Annee> lesAnnees = AnneeDAO.selectAll();
        if (!lesAnnees.isEmpty()) {
            //Remise à zéro des JComboBox
            this.vue.getjComboBoxAnneeDebut().setModel(new javax.swing.DefaultComboBoxModel());
            this.vue.getjComboBoxAnneeFin().setModel(new javax.swing.DefaultComboBoxModel());

            //Remplissage des JComboBox avec les années
            for (Annee uneAnne : lesAnnees) {
                this.vue.getjComboBoxAnneeDebut().addItem(Integer.toString(uneAnne.getDateAnnee()));
                this.vue.getjComboBoxAnneeFin().addItem(Integer.toString(uneAnne.getDateAnnee()));

            }
            //Sélection des Années de début et de fin par défaut
            this.vue.getjComboBoxAnneeDebut().setSelectedIndex(0);
            this.vue.getjComboBoxAnneeFin().setSelectedIndex(this.vue.getjComboBoxAnneeDebut().getItemCount() - 1);

            // Remplissage de la Table Contact Accord Stagiaires
            afficherLesProfessionnelsStagiaires();
        }
    }

    /**
     * Remplissage de la table Accord Stagiaire
     */
    private void afficherLesProfessionnelsStagiaires() {
        ArrayList<Professionnel> lesProfessionnels;
        JFrame frame = new JFrame("showMessageDialog");
        try {
            //Si Année début inférieur à Année fin
            if (Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()) - Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()) <= 0) {   
                
                    //Si le Radio Bouton Tous est sélectionné pour période et spécialité                
                if (getVue().getjRadioButtonToutePeriode().isSelected() && getVue().getjRadioButtonTouteSpecialite().isSelected()) {
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordStageAnnee(Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()));
                    
                    //sinon si on sélectionne une période
                } else if (!getVue().getjRadioButtonToutePeriode().isSelected() && getVue().getjRadioButtonTouteSpecialite().isSelected()) {
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordStageAnneePerio(perio, Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()));
                    
                    //sinon si on sélectionne une spécialité
                } else if (getVue().getjRadioButtonToutePeriode().isSelected() && !getVue().getjRadioButtonTouteSpecialite().isSelected()) {
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordStageAnneeSpe(spe, Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()));
                    
                    //sinon si on sélectionne une période et une spécialité
                } else {
                    lesProfessionnels = ProfessionnelDAO.selectProfByAccordStageAll(spe, Integer.parseInt(this.vue.getjComboBoxAnneeDebut().getSelectedItem().toString()), Integer.parseInt(this.vue.getjComboBoxAnneeFin().getSelectedItem().toString()), perio);
                }
                
                //on remplit les colonnes
                vue.getModeleTableStage().setRowCount(0);
                String[] titresColonnes = {"Nom", "Prénom", "Spécialité", "Téléphone", "Email ", "Nom Entreprise", "Adresse Entreprise"};
                vue.getModeleTableStage().setColumnIdentifiers(titresColonnes);
                String[] ligneDonnees = new String[7];
                for (Professionnel unProfessionnel : lesProfessionnels) {
                    ligneDonnees[0] = unProfessionnel.getNom();
                    ligneDonnees[1] = unProfessionnel.getPrenom();
                    ligneDonnees[2] = unProfessionnel.getSpe();
                    ligneDonnees[3] = unProfessionnel.getTel();
                    ligneDonnees[4] = unProfessionnel.getMail();
                    ligneDonnees[5] = unProfessionnel.getEntreprise().getNom();
                    ligneDonnees[6] = unProfessionnel.getEntreprise().getAdr() + ", " + unProfessionnel.getEntreprise().getCp() + " " + unProfessionnel.getEntreprise().getVille();
                    vue.getModeleTableStage().addRow(ligneDonnees);
                }
                //Si la Table ProfStage ne retourne aucune ligne
                if (vue.getModeleTableStage().getRowCount() == 0) {
                    JOptionPane.showMessageDialog(frame, "Aucune donnée pour ces paramètres", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
                // Redimensionner table en fonction de son contenu
                ctrlGeneral.resizeColumnWidth(vue.getjTableStage());
            } else { //Si Année début supérieur à Année fin
                JOptionPane.showMessageDialog(frame, "Année début supérieur à Année fin", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getVue(), "CtrlLesAccordsDtagiaire - échec de sélection des Accords Stagiaire, erreur : " + ex);
        }
    }

    // ACCESSEURS et MUTATEURS
    public VueContactAccordStagiaires getVue() {
        return vue;
    }

    public void setVue(VueContactAccordStagiaires vue) {
        this.vue = vue;
    }

    //Méthodes Abstraites (WindowListener)
    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        ctrlGeneral.quitterApplication();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    //Méthode Abstraite (ActionListener)
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(vue.getjButtonRetour())) { //Bouton Retour
            ctrlGeneral.afficherMain();
        }
        if (e.getSource().equals(vue.getjButtonRefresh())) { //Bouton Rechercher / Rafraichir
            afficherLesProfessionnelsStagiaires();
        }
        if (e.getSource().equals(vue.getjRadioButtonSLAM())) { //RadioBouton SLAM
            spe = "SLAM";
        }
        if (e.getSource().equals(vue.getjRadioButtonSISR())) { //RadioBouton SISR
            spe = "SISR";
        }
        if (e.getSource().equals(vue.getjRadioButtonPremierSemestre())) { //RadioBouton PremierSemestre
            perio = "accord1ereStage";        }
        if (e.getSource().equals(vue.getjRadioButtonSecondSemestre())) { //RadioBouton SecondSemestre
            perio = "accord2emeStage";
        }
    }
}
