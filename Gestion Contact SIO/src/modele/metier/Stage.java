package modele.metier;

import java.util.ArrayList;

/**
 * Classe Métier Stage
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class Stage {

    private int id, anneeStage, periodeStage;
    private String unSujetStage;
    private Eleve unEleve;
    private Entreprise uneEntreprise;
    private Professionnel unProfessionnel;
    private Technologie uneTechnologie;
    private ArrayList<Visite> lesVisites;

    /**
     * Constructeur Stage (avec paramètres)
     *
     * @param unId Identifiant du stage
     * @param uneAnnee Année du stage
     * @param unePeriode période du stage (en semaines)
     * @param sujetStage
     * @param unEleve
     * @param uneEntreprise
     * @param unProfessionnel
     * @param uneTechnologie
     * @param lesVisites Liste des visites du stage
     */
    public Stage(int unId, int uneAnnee, int unePeriode, String sujetStage, Eleve unEleve, Entreprise uneEntreprise, Professionnel unProfessionnel, Technologie uneTechnologie, ArrayList<Visite> lesVisites) {
        this.id = unId;
        this.anneeStage = uneAnnee;
        this.periodeStage = unePeriode;
        this.unSujetStage = sujetStage;
        this.unEleve = unEleve;
        this.uneEntreprise = uneEntreprise;
        this.unProfessionnel = unProfessionnel;
        this.uneTechnologie = uneTechnologie;
        this.lesVisites = lesVisites;
    }

    /**
     * Constructeur Stage (sans paramètre)
     */
    public Stage() {
    }

    @Override
    public String toString() {
        return "Id Stage: " + this.id + ", Année de stage: " + this.anneeStage + ", Nombre de semaine du stage: " + this.periodeStage + ", Sujet du stage: " + this.unSujetStage + ", L'élève stagiaire: " + this.unEleve + ", L'entreprise: " + this.uneEntreprise + ", Le maitre de stage: " + this.unProfessionnel + ", Technologie étudiée: " + this.uneTechnologie +", Les Visites: " + this.lesVisites;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnneeStage() {
        return anneeStage;
    }

    public void setAnneeStage(int anneeStage) {
        this.anneeStage = anneeStage;
    }

    public int getPeriodeStage() {
        return periodeStage;
    }

    public void setPeriodeStage(int periodeStage) {
        this.periodeStage = periodeStage;
    }

    public ArrayList<Visite> getLesVisites() {
        return lesVisites;
    }

    public void setLesVisites(ArrayList<Visite> lesVisites) {
        this.lesVisites = lesVisites;
    }

    public void ajouterVisite(Visite vi) {
        lesVisites.add(vi);
    }

    public Eleve getUnEleve() {
        return unEleve;
    }

    public void setUnEleve(Eleve unEleve) {
        this.unEleve = unEleve;
    }

    public Entreprise getUneEntreprise() {
        return uneEntreprise;
    }

    public void setUneEntreprise(Entreprise uneEntreprise) {
        this.uneEntreprise = uneEntreprise;
    }

    public Professionnel getUnProfessionnel() {
        return unProfessionnel;
    }

    public void setUnProfessionnel(Professionnel unProfessionnel) {
        this.unProfessionnel = unProfessionnel;
    }

    public String getUnSujetStage() {
        return unSujetStage;
    }

    public void setUnSujetStage(String unSujetStage) {
        this.unSujetStage = unSujetStage;
    }

    public Technologie getUneTechnologie() {
        return uneTechnologie;
    }

    public void setUneTechnologie(Technologie uneTechnologie) {
        this.uneTechnologie = uneTechnologie;
    }

}
