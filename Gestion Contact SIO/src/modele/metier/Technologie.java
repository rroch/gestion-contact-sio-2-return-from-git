/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Technologie {
    private int id;
    private String libelle;

    
    /**
     * Constructeur Professionnel (avec paramètres)
     *
     * @param id Identifiant de la technologie
     * @param libelle Libelle de la technologie
     */
    public Technologie(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }
    
    public Technologie() {
    }

    @Override
    public String toString() {
        return "Technologie : " + this.id + ", Libelle = " + this.libelle;
    }
    
    // ACCESSEURS et MUTATEURS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
