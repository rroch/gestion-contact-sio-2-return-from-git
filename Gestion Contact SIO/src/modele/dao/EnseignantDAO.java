package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Enseignant;

/**
 * Classe DAO Enseignant
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class EnseignantDAO {

    /**
     * selectOne : lire un enregistrement dans la table ENSEIGNANT
     *
     * @param idEnseignant identifiant de l'enseignant
     * @return une instance de la classe métier Enseignant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Enseignant selectOne(int idEnseignant) throws SQLException {
        Enseignant unEnseignant = new Enseignant();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ENSEIGNANTS WHERE IDENSEI= ?");
        pstmt.setInt(1, idEnseignant);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            unEnseignant = enseignantFromResultSet(rs);
        }
        return unEnseignant;
    }

    /**
     * selectAll : lire tous les enregistrements de la table ENSEIGNANT
     *
     * @return une collection d'instances de la classe Enseignant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Enseignant> selectAll() throws SQLException {
        ArrayList<Enseignant> lesEnseignants = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENSEIGNANTS";
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement(requete);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Enseignant unEnseignant = enseignantFromResultSet(rs);
            lesEnseignants.add(unEnseignant);
        }
        return lesEnseignants;
    }

    /**
     * Extrait un enregistrement du "ResultSet" issu de la table ENSEIGNANT
     *
     * @param rs : ResultSet lu dans la table ENSEIGNANT
     * @return instance de Enseignant, initialisée d'après le premier
     * enregistrement du ResultSet
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    private static Enseignant enseignantFromResultSet(ResultSet rs) throws SQLException {
        Enseignant unEnseignant = new Enseignant();
        unEnseignant.setId(rs.getInt("idEnsei"));
        unEnseignant.setNom(rs.getString("nomEnsei"));
        unEnseignant.setPrenom(rs.getString("prenomEnsei"));
        unEnseignant.setLesVisites(VisiteDAO.selectAllByEnseignant(rs.getInt("idEnsei")));
        return unEnseignant;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table ENSEIGNANT
     *
     * @param idEnsei Identifiant de l'enseignant à ajouter
     * @param nomEnsei Nom de l'enseignant
     * @param prenomEnsei Prénom de l'enseignant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idEnsei, String nomEnsei, String prenomEnsei) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO ENSEIGNANTS (IDENSEI, NOMENSEI, PRENOMENSEI) VALUES (?, ?, ?)");
        pstmt.setInt(1, idEnsei);
        pstmt.setString(2, nomEnsei);
        pstmt.setString(3, prenomEnsei);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table ENSEIGNANT
     *
     * @param idEnsei Identifiant de l'enseignant à modifier
     * @param nomEnsei Nom de l'enseignant
     * @param prenomEnsei Prénom de l'enseignant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idEnsei, String nomEnsei, String prenomEnsei) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE ENSEIGNANTS SET NOMENSEI = ? , PRENOMENSEI = ? WHERE IDENSEI = ?");
        pstmt.setString(1, nomEnsei);
        pstmt.setString(2, prenomEnsei);
        pstmt.setInt(3, idEnsei);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table ENSEIGNANT
     *
     * @param idEnseignant Identifiant de l'enseignant à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idEnseignant) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM ENSEIGNANTS WHERE IDENSEI = ?");
        pstmt.setInt(1, idEnseignant);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
