package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Visite;

/**
 * Classe DAO Visite
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class VisiteDAO {

    /**
     * selectOne : lire un enregistrement dans la table VISITE
     *
     * @param idVisite Identifiant de la visite
     * @return une instance de la classe métier Visite
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Visite selectOne(int idVisite) throws SQLException {
        Visite uneVisite = new Visite();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM VISITE WHERE IDVISITE= ?");
        pstmt.setInt(1, idVisite);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            uneVisite = new Visite(rs.getInt("IDVISITE"), rs.getBoolean("ACCORDJURY"), rs.getBoolean("ACCORD1ERESTAGE"), rs.getBoolean("ACCORD2EMESTAGE"));
        }
        return uneVisite;
    }

    /**
     * selectAll : lire tous les enregistrements de la table VISITE
     *
     * @return une collection d'instances de la classe Visite
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Visite> selectAll() throws SQLException {
        ArrayList<Visite> lesVisites = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM VISITE");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Visite uneVisite = new Visite(rs.getInt("IDVISITE"), rs.getBoolean("ACCORDJURY"), rs.getBoolean("ACCORD1ERESTAGE"), rs.getBoolean("ACCORD2EMESTAGE"));
            lesVisites.add(uneVisite);
        }
        return lesVisites;
    }

    public static ArrayList<Visite> selectAllByEnseignant(int idEnseignant) throws SQLException {
        ArrayList<Visite> lesVisites = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM VISITE WHERE idEnsei = ?");
        pstmt.setInt(1, idEnseignant);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Visite uneVisite = new Visite(rs.getInt("IDVISITE"), rs.getBoolean("ACCORDJURY"), rs.getBoolean("ACCORD1ERESTAGE"), rs.getBoolean("ACCORD2EMESTAGE"));
            lesVisites.add(uneVisite);
        }
        return lesVisites;
    }

    public static ArrayList<Visite> selectAllByStage(int idStage) throws SQLException {
        ArrayList<Visite> lesVisites = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM VISITE WHERE idStage = ?");
        pstmt.setInt(1, idStage);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Visite uneVisite = new Visite(rs.getInt("IDVISITE"), rs.getBoolean("ACCORDJURY"), rs.getBoolean("ACCORD1ERESTAGE"), rs.getBoolean("ACCORD2EMESTAGE"));
            lesVisites.add(uneVisite);
        }
        return lesVisites;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table Visite
     *
     * @param idVisite Identifiant de la Visite à ajouter
     * @param accordJury Accord pour participer au jury
     * @param accord1ereStage Accord pour prendre des stagiaires en première
     * année
     * @param idStage Identifiant du stage
     * @param idEnsei Identifiant de l'enseignant
     * @param accord2emeStage Accord pour prendre des stagiaires en deuxième
     * année
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(int idVisite, boolean accordJury, boolean accord1ereStage, boolean accord2emeStage, int idStage, int idEnsei) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO VISITE (IDVISITE, ACCORDJURY, ACCORD1ERESTAGE, ACCORD2EMESTAGE,IDSTAGE,IDENSEI) VALUES (?,?,?,?,?,?)");
        pstmt.setInt(1, idVisite);
        pstmt.setBoolean(2, accordJury);
        pstmt.setBoolean(3, accord1ereStage);
        pstmt.setBoolean(4, accord2emeStage);
        pstmt.setInt(5, idStage);
        pstmt.setInt(6, idEnsei);

        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table VISITE
     *
     * @param idVisite Identifiant de la Visite à modifier
     * @param accordJury Accord pour participer au jury
     * @param accord1ereStage Accord pour prendre des stagiaires en première
     * année
     * @param accord2emeStage Accord pour prendre des stagiaires en deuxième
     * année
     * @return 1 si l'enregistrement a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(int idVisite, boolean accordJury, boolean accord1ereStage, boolean accord2emeStage) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE VISITE SET ACCORDJURY = ?,ACCORD1ERESTAGE = ?, ACCORD2EMESTAGE = ? WHERE IDVISITE = ?");
        pstmt.setBoolean(1, accordJury);
        pstmt.setBoolean(2, accord1ereStage);
        pstmt.setBoolean(3, accord2emeStage);
        pstmt.setInt(4, idVisite);

        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table VISITE
     *
     * @param idVisite : Identifiant de la visite à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int idVisite) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM VISITE WHERE IDVISITE = ?");
        pstmt.setInt(1, idVisite);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
