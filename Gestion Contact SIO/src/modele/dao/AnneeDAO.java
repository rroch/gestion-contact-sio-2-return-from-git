package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Annee;

/**
 * Classe DAO Année
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class AnneeDAO {

    /**
     * selectOne : lire un enregistrement dans la table ANNEE
     *
     * @param dateAnnee : identifiant de l'Annee recherché
     * @return une instance de la classe métier Annee
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static Annee selectOne(int dateAnnee) throws SQLException {
        Annee uneAnnee = new Annee();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ANNEE WHERE DATEANNEE= ?");
        pstmt.setInt(1, dateAnnee);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            uneAnnee = new Annee(rs.getInt("DATEANNEE"));
        }
        return uneAnnee;
    }

    /**
     * selectAll : lire tous les enregistrements de la table ANNEE
     *
     * @return une collection d'instances de la classe Annee
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static ArrayList<Annee> selectAll() throws SQLException {
        ArrayList<Annee> lesAnnees = new ArrayList<>();
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("SELECT * FROM ANNEE");
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Annee uneAnnee = new Annee(rs.getInt("DATEANNEE"));
            lesAnnees.add(uneAnnee);
        }
        return lesAnnees;
    }

    /**
     * Insert : Ajouter un enregistrement dans la table ANNEE
     *
     * @param uneAnnee : instance de la classe Annee à enregistrer dans la table
     * ANNEE
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int insert(Annee uneAnnee) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("INSERT INTO ANNEE (DATEANNEE) VALUES (?)");
        pstmt.setInt(1, uneAnnee.getDateAnnee());
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Update : Modifier un enregistrement de la table ANNEE
     *
     * @param dateAnnee Identifiant conceptuel de l'Annee à modifier
     * @param uneAnnee Instance de la classe Annee contenant les nouvelles
     * valeurs à enregistrer dans la table ANNEE sous le même identifiant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int update(Annee uneAnnee, String dateAnnee) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("UPDATE ANNEE SET DATEANNEE = ? WHERE DATEANNEE = ?");
        pstmt.setInt(1, uneAnnee.getDateAnnee());
        pstmt.setString(2, dateAnnee);
        int nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * Delete : Supprimer un enregistrement de la table ANNEE
     *
     * @param dateAnnee : Identifiant de l'annee à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static int delete(int dateAnnee) throws SQLException {
        Jdbc jdbc = Jdbc.getInstance();
        PreparedStatement pstmt = jdbc.getConnexion().prepareStatement("DELETE FROM ANNEE WHERE DATEANNEE = ?");
        pstmt.setInt(1, dateAnnee);
        int nb = pstmt.executeUpdate();
        return nb;
    }
}
