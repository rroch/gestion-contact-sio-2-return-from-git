package lanceur;

import modele.dao.Jdbc;
import vue.*;
import controlleur.*;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * Lanceur Application
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV"); // 
        try {
            Jdbc.getInstance().connecter();
            GeneralControlleur leControleurGeneral = new GeneralControlleur();

            // instanciation contrôleur et vue Main
            VueMain laVueMain = new VueMain();
            MainControlleur leControleurMain = new MainControlleur(laVueMain, leControleurGeneral);
            leControleurGeneral.setCtrlMain(leControleurMain);

            // instanciation contrôleur et vue ContactAccordJury
            VueContactAccordJury laVueContactAccordJury = new VueContactAccordJury();
            ContactAccordJuryControlleur leControleurContactAccordJury = new ContactAccordJuryControlleur(laVueContactAccordJury, leControleurGeneral);
            leControleurGeneral.setCtrlContactAccordJury(leControleurContactAccordJury);

            // instanciation contrôleur et vue ListeStage
            VueListeStage laVueListeStage = new VueListeStage();
            ListeStageControlleur leControleurListeStageControlleur = new ListeStageControlleur(laVueListeStage, leControleurGeneral);
            leControleurGeneral.setCtrlListeStage(leControleurListeStageControlleur);

            // instanciation contrôleur et vue ContactAccordStagiaires
            VueContactAccordStagiaires laVueContactAccordStagiaires = new VueContactAccordStagiaires();
            ContactAccordStagiairesControlleur leControleurContactAccordStagiaires = new ContactAccordStagiairesControlleur(laVueContactAccordStagiaires, leControleurGeneral);
            leControleurGeneral.setCtrlContactAccordStagiaires(leControleurContactAccordStagiaires);

            // instanciation contrôleur et vue StagiairesEncadres
            VueStagiairesEncadres laVueStagiairesEncadres = new VueStagiairesEncadres();
            StagiairesEncadresControlleur leControleurStagiairesEncadres = new StagiairesEncadresControlleur(laVueStagiairesEncadres, leControleurGeneral);
            leControleurGeneral.setCtrlStagiairesEncadres(leControleurStagiairesEncadres);

            // afficher la vue initiale
            laVueMain.setVisible(true);

        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Main - classe JDBC non trouvée (mysql-connector-java-8.0.18.jar)");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Main - échec de connexion");
        }

    }

}
