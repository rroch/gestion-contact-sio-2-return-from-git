package test.metier;

import java.util.ArrayList;
import modele.metier.Entreprise;
import modele.metier.Visite;

/**
 * Test Métier Entreprise
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestEntreprise {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Entreprise === \n");
        System.out.println("Test d'instanciation et d'état");
        ArrayList<Visite> lesVisites = new ArrayList<>();
        lesVisites.add(new Visite(1, false, false, true));
        Entreprise entr = new Entreprise(1, "Ardelice", "07-83-12-18-42", "7 rue de la boulangerie", 44120, "Vertou");
        System.out.println("Etat de l'entreprise : " + entr.toString());
    }
}
