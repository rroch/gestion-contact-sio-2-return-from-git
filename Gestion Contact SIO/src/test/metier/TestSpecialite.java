package test.metier;

import java.util.ArrayList;
import modele.metier.Annee;
import modele.metier.Eleve;
import modele.metier.Specialite;
import modele.metier.Visite;

/**
 * Test Métier Spécialité
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestSpecialite {

    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Spécialité === \n");
        System.out.println("Test d'instanciation et d'état");
        ArrayList<Visite> lesVisites = new ArrayList<>();
        lesVisites.add(new Visite(1, false, false, true));
        Eleve el = new Eleve(1, "Baudry", "Clément");
        Specialite spe = new Specialite(1, "SLAM", new Annee(2020), el);
        System.out.println("Etat de la specialite : " + spe.toString());
    }
}
