package test.metier;

import modele.metier.Technologie;

/**
 * Test Métier Technologie
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestTechnologie {
    
    public static void main(String[] args) {
        // Test n°1 - Test d'instanciation et d'état
        System.out.println("\n === Test Technologie === \n");
        System.out.println("Test d'instanciation et d'état");
        Technologie tech = new Technologie(1, "Webdev");
        System.out.println("Etat de la technologie : " + tech.toString());
    }
}
