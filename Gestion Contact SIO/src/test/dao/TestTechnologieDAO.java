package test.dao;

import java.sql.SQLException;
import java.util.List;
import modele.dao.Jdbc;
import modele.dao.TechnologieDAO;
import modele.metier.Technologie;

/**
 * Test TechnologieDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestTechnologieDAO {
    public static void main(String[] args) {

        java.sql.Connection cnx = null;

        try {
            System.out.println("\n === Test DAO Technologie === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(4);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            test3_Insert(999, "Windows");
            System.out.println("Test 3 effectué : insertion\n");
            test4_Update(999, "Linux");
            System.out.println("Test 4 effectué : modification\n");
            test5_Delete(999);
            System.out.println("Test 4 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV");
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche une Technologie d'après son identifiant
     *
     * @param id Identifiant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int id) throws SQLException {
        Technologie cetTech = TechnologieDAO.selectOne(id);
        if (cetTech != null) {
            System.out.println("Technologie d'identifiant : " + id + " : " + cetTech.toString());
        } else {
            System.out.println("La technologie d'identifiant : " + id + " n'existe pas ");
        }

    }

    /**
     * Affiche toutes les Technologies
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Technologie> desTechs = TechnologieDAO.selectAll();
        System.out.println("Les technologies lues : " + desTechs.toString());
    }

    /**
     * Ajoute une Technologie
     *
     * @param id Identifiant
     * @param libelle Libellé
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int id, String libelle) throws SQLException {
        int nb = TechnologieDAO.insert(id, libelle);
        System.out.println("Une nouvelle technologie a été insérée: " + nb);
        test2_SelectMultiple();
    }
    
    /**
     * Modifie une Technologie
     * 
     * @param id Identifiant
     * @param libelle Libellé
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int id, String libelle) throws SQLException {
        int nb = TechnologieDAO.update(id, libelle);
        System.out.println("La technologie a été modifiée : " + nb);
        test2_SelectMultiple();
    }

    /**
     * Supprime une Technologie
     *
     * @param id Identifiant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test5_Delete(int id) throws SQLException {
        int nb = TechnologieDAO.delete(id);
        System.out.println("Une technologie a été supprimée: " + nb);
        test2_SelectMultiple();
    }
}
