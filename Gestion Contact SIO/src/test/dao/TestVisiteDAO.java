package test.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import modele.metier.Visite;
import modele.dao.Jdbc;
import modele.dao.VisiteDAO;

/**
 * Test VisiteDAO
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestVisiteDAO {

    public static void main(String[] args) throws SQLException {

        java.sql.Connection cnx = null;

        try {
            System.out.println("\n === Test DAO Visite === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : Connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : Sélection unique\n");
            test21_SelectMultiple();
            System.out.println("Test 2.1 effectué : Sélection multiple\n");
            test22_SelectByEnseignant(2);
            System.out.println("Test 2.2 effectué : Sélection par enseignant\n");
            test3_Insert(999, true, true, true, 2, 3);
            System.out.println("Test 3 effectué : Insertion\n");
            test4_Update(999, true, false, false);
            System.out.println("Test 4 effectué : Mise à jour\n");
            test5_Delete(999);
            System.out.println("Test 5 effectué : Suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV");
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche une visite d'après son identifiant
     *
     * @param idVisite
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int idVisite) throws SQLException {
        Visite laVisite = VisiteDAO.selectOne(idVisite);
        if (laVisite != null) {
            System.out.println("Visite d'identifiant : " + idVisite + " : " + laVisite.toString());
        } else {
            System.out.println("La visite d'identifiant : " + idVisite + " n'existe pas ");
        }
    }

    /**
     * Affiche toutes les visites
     *
     * @throws SQLException
     */
    public static void test21_SelectMultiple() throws SQLException {
        ArrayList<Visite> desVisites = (ArrayList<Visite>) VisiteDAO.selectAll();
        if (desVisites != null) {
            System.out.println("Les visites lues : " + desVisites.toString());
        } else {
            System.out.println("La table visite est nulle");
        }
    }

    /**
     * Affiche toutes les visites par rapport à un enseignant
     *
     * @param idEnseignant
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test22_SelectByEnseignant(int idEnseignant) throws SQLException {
        ArrayList<Visite> desVisites = (ArrayList<Visite>) VisiteDAO.selectAllByEnseignant(idEnseignant);
        if (desVisites != null) {
            System.out.println("Les visites de l'enseignant " + idEnseignant + " : " + desVisites.toString());
        } else {
            System.out.println("La table visite est nulle");
        }
    }

    /**
     * Ajoute une Visite
     *
     * @param idVisite
     * @param accordJury
     * @param accordPremStage
     * @param accordSecStage
     * @param idStage
     * @param idEnsei
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int idVisite, boolean accordJury, boolean accordPremStage, boolean accordSecStage, int idStage, int idEnsei) throws SQLException {
        int nb = VisiteDAO.insert(idVisite, accordJury, accordPremStage, accordSecStage, idStage, idEnsei);
        System.out.println("Une nouvelle visite a été inséré: " + nb);
        test21_SelectMultiple();
    }

    /**
     * Modifie une Visite
     *
     * @param idVisite
     * @param accordJury
     * @param accordPremStage
     * @param accordSecStage
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int idVisite, boolean accordJury, boolean accordPremStage, boolean accordSecStage) throws SQLException {
        int nb = VisiteDAO.update(idVisite, accordJury, accordPremStage, accordSecStage);
        System.out.println("La visite " + idVisite + " a été mis à jour: " + nb);
        test1_SelectUnique(idVisite);
    }

    /**
     * Supprime une Visite
     *
     * @param idVisite Id
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test5_Delete(int idVisite) throws SQLException {
        int nb = VisiteDAO.delete(idVisite);
        System.out.println("La visite a été supprimée: " + nb);
        test21_SelectMultiple();
    }
}
