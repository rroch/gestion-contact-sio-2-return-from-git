package test.dao;

import java.sql.Connection;
import java.sql.SQLException;
import modele.dao.Jdbc;
import modele.dao.SpecialiteDAO;

/**
 * Test SpecialiteDAO
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestSpecialiteDAO {

    public static void main(String[] args) {
        java.sql.Connection cnx = null;
        try {
            System.out.println("\n === Test DAO Specialité === \n");
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            test3_Insert(50, "SLAM", 2020, 10);
            System.out.println("Test 3 effectué : insertion\n");
            test4_Update(50, "SISR");
            System.out.println("Test 4 effectué : modification\n");
            test5_Delete(50);
            System.out.println("Test 5 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD (Utilise
     * mysql-connector-java-8.0.18.jar)
     *
     * @throws ClassNotFoundException
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "91.160.18.96:3306/", "prj2eq10_gestioncontactsio?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC", "prj2eq10", "n6vEXV");
        Jdbc.getInstance().connecter();
    }

    /**
     * Affiche une Specialite d'après son identifiant
     *
     * @param idSpecialite
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test1_SelectUnique(int idSpecialite) throws SQLException {
        if (SpecialiteDAO.selectOne(idSpecialite) != null) {
            System.out.println("Specialite d'identifiant : " + idSpecialite + " : " + SpecialiteDAO.selectOne(idSpecialite).toString());
        } else {
            System.out.println("La spécialité d'identifiant : " + idSpecialite + " n'existe pas ");
        }
    }

    /**
     * Affiche toutes les Spécialités
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test2_SelectMultiple() throws SQLException {
        System.out.println("Les spécialités lus : " + SpecialiteDAO.selectAll().toString());
    }

    /**
     * Ajoute une Specialite
     *
     * @param idSpecialite
     * @param libelleSpecialite
     * @param dateAnnee
     * @param idEleve
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test3_Insert(int idSpecialite, String libelleSpecialite, int idEleve, int dateAnnee) throws SQLException {
        System.out.println("Une nouvelle Specialite a été inséré: " + SpecialiteDAO.insert(idSpecialite, libelleSpecialite, dateAnnee, idEleve));
        test2_SelectMultiple();
    }

    /**
     * Modifie une Specialite
     *
     * @param idSpecialite
     * @param libelleSpecialite
     *
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test4_Update(int idSpecialite, String libelleSpecialite) throws SQLException {
        System.out.println("La spécialité " + idSpecialite + " a été mis à jour: " + SpecialiteDAO.update(idSpecialite, libelleSpecialite));
        test1_SelectUnique(idSpecialite);
    }

    /**
     * Supprime une Specialite
     *
     * @param idSpecialite
     * @throws SQLException Retourne une Exception SQL si une erreur est trouvé
     */
    public static void test5_Delete(int idSpecialite) throws SQLException {
        System.out.println("Une spécialité a été supprimée: " + SpecialiteDAO.delete(idSpecialite));
        test2_SelectMultiple();
    }
}
