package test.csv;

import csv.CsvFileHelper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test CSV
 * 
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class TestCsvFileHelper {

    private final static String READ_FILE_NAME = "src/test/csv/Export_Stages_20200310_entreprises.csv";
    private final static String WRITE_FILE_NAME_1 = "src/test/csv/WriteTest.csv";
    private final static String WRITE_FILE_NAME_2 = "src/test/csv/ReadWriteTest.csv";

    public static void main(String[] args) throws IOException {
        TestCsvFileHelper csvFileHelperTest = new TestCsvFileHelper();
    }

    public TestCsvFileHelper() throws IOException {
        testGetResource();
        testReadFile();
        testReadCsvFile();
        testWriteCsvFile();
        testReadWriteCsvFile();
    }

    public void testGetResource() {
        final String fileName = READ_FILE_NAME;
        final File file = CsvFileHelper.getResource(fileName);
        // On sait que le fichier existe bien puisque c'est avec lui qu'on travaille depuis le début.
        System.out.println("diagnostic testGetResource :" + file.exists());
    }

    public void testReadFile() {
        boolean diag = true;
        String msg = "";
        try {
            // Param
            final String fileName = READ_FILE_NAME;

            // Result
            final int nombreLignes = 32;

            // Appel
            final File file = CsvFileHelper.getResource(fileName);
            List<String> lines = CsvFileHelper.readFile(file);

            // Test
            diag = (nombreLignes == lines.size());
        } catch (IOException ex) {
            diag = false;
            msg = ex.getMessage();
        } finally {
            System.out.println("diagnostic testCsvFile :" + diag + " : " + msg);

        }

    }

    public void testReadCsvFile() {
        boolean diag = true;
        String msg = "";
        try {
            // Init
            final String fileName = READ_FILE_NAME;
            final File file = CsvFileHelper.getResource(fileName);
            final List<String> lines = CsvFileHelper.readFile(file);
            // Appel de la méthode à tester            
            final List<String[]> data = CsvFileHelper.readCsvFile(file, ';');
            // Test 1 : le nombre de lignes correspond
            diag = diag && (32 == data.size());
            // Test 2 : le nombre de  colonnes correspond
            for (String[] oneData : data) {
                // test 2-i : pour chaque ligne
                diag = diag && (6 == oneData.length);
                // Pour chaque champ, on affche la donnée (test visuel)
                for (String string : oneData) {
                    System.out.print(string + " - ");
                }
                System.out.println("");
            }
            // Tests 3 à 8 : les noms des colonnes correspondent
            String[] titres = data.get(0);
            diag = diag && (titres[0].equals("no"));
            diag = diag && (titres[1].equals("Nom"));
            diag = diag && (titres[2].equals("Prenom"));
            diag = diag && (titres[3].equals("Login"));
            diag = diag && (titres[4].equals("Email"));
            diag = diag && (titres[5].equals("Groupe"));
        } catch (IOException ex) {
            diag = false;
            msg = ex.getMessage();
        } finally {
            System.out.println("diagnostic testReadCsvFile :" + diag + " : " + msg);

        }

    }

    public void testWriteCsvFile() {
        boolean diag = true;
        String msg = "";
        try {
            // Init
            final String fileName = WRITE_FILE_NAME_1;
            final File file = CsvFileHelper.getResource(fileName);
            final List<String[]> data = new ArrayList<String[]>();
            String[] titles = {"Numero", "Nom", "Prenom", "Age"};
            String[] line = new String[4];
            data.add(titles);
            line[0] = "1";
            line[1] = "DUPONT";
            line[2] = "Sarah";
            line[3] = "25";
            data.add(line);
            line = new String[4];
            line[0] = "2";
            line[1] = "BRUAND";
            line[2] = "Charles";
            line[3] = "61";
            data.add(line);
            line = new String[4];
            line[0] = "3";
            line[1] = "PORTIER";
            line[2] = "Alexa";
            line[3] = "48";
            data.add(line);

            CsvFileHelper.writeCsvFile(file, ';', data);

        } catch (IOException ex) {
            diag = false;
            msg = ex.getMessage();
        } finally {
            System.out.println("diagnostic testWriteCsvFile n°1 :" + diag + " : " + msg);

        }
    }

    public void testReadWriteCsvFile() {
        boolean diag = true;
        String msg = "";
        try {
            String fichierCsvLecture = READ_FILE_NAME;
            File ficIn = CsvFileHelper.getResource(fichierCsvLecture);
            String fichierCsvEcriture = WRITE_FILE_NAME_2;
            File ficOut = CsvFileHelper.getResource(fichierCsvEcriture);

            List<String[]> data = CsvFileHelper.readCsvFile(ficIn, ';');
            CsvFileHelper.writeCsvFile(ficOut, '-', data);
        } catch (IOException ex) {
            diag = false;
            msg = ex.getMessage();
        } finally {
            System.out.println("diagnostic testReadWriteCsvFile :" + diag + " : " + msg);
        }

    }

}
