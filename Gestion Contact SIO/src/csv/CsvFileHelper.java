package csv;

import java.io.*;
import java.util.*;

/**
 * Classe offrant des méthodes d'import/export de fichiers CSV
 * @author nbourgeois
 * @version mai 2019
 * A partir de l'article de Thierry Leriche-Dessirier sur developpez.com
 * https://thierry-leriche-dessirier.developpez.com/tutoriels/java/csv-avec-java
 * 
 */
public class CsvFileHelper {
    
    /**
     * Construction d'un chemin d'accès complet d'après un nom de fichier
     * @param fileName
     * @return 
     */
    public static String getResourcePath(String fileName) {
        final File f = new File("");
        final String dossierPath = f.getAbsolutePath() + File.separator + fileName;
        return dossierPath;
    }

    /**
     * Création d'un objet File d'après le nom du fichier
     * @param fileName nom du fichier
     * @return 
     */
    public static File getResource(String fileName) {
        final String completeFileName = getResourcePath(fileName);
        File file = new File(completeFileName);
        return file;
    }  
    
    /**
     *  Lecture d'un fichier et décomposition ligne par ligne
     * @param file fichier de texte
     * @return collection : chaque élément est une chaîne de caractères représentant une ligne du fichier
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static List<String> readFile(File file) throws FileNotFoundException, IOException {

        List<String> result = new ArrayList<String>();

        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);

        for (String line = br.readLine(); line != null; line = br.readLine()) {
            result.add(line);
        }

        br.close();
        fr.close();

        return result;
    }
    
    /**
     * Lecture et analyse d'un fichier csv
     * @param file
     * @param separator séparateur choisi pour le csv (';' ',' ...)
     * @return collection : chaque élément est tableau représentant un enregistrement ; chaque case du tableau est une valeur
     * @throws IOException 
     */
    public static List<String[]> readCsvFile(File file, char separator) throws IOException {
        List<String[]> data = new ArrayList<>();
        // Lire le fichier, le résultat est dans une collection de lignes
        List<String> lines = readFile(file);
        String sep = new Character(separator).toString();
        // traitement ligne par ligne en les décomposant en valeurs délimitées par le séparateur
        for (String line : lines) {
            String[] oneData = line.split(sep);
            data.add(oneData);
        }
        return data;
    }
    
    /**
     * Création d'un fichier Csv d'après une collection d'enregistrements
     * @param file Fichier à créer
     * @param separator séparateur à utiliser
     * @param lines collection d'enregistrements
     * @throws IOException 
     */
    public static void writeCsvFile(File file, char separator, List<String[]> lines) throws IOException{
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        int i;

        // Pour chaque ligne de la collection
        for(String[] line : lines){
            // Pour chaque mot (valeur) du tableau représentant une ligne
            // on s'arrête à l'avant dernier mot (length -1)
            for(i=0; i<line.length-1;i++){
                String word = line[i];
                // écrire la valeur et son séparateur
                bw.write(word);
                bw.write(separator);
            }
            // le dernier mot n'est pas suivi du séparateur, mais d'un saut de ligne
            if(i>0){
                bw.write(line[i]);
                bw.write("\n");
            }  
        }
        bw.close();
        fw.close();
    }
    
}
